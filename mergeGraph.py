from mergeSort import mergeSort
import random
from time import time
import matplotlib.pyplot as plt

n = 10000
i = 0
inputSizes = []
mergeBestTimes = []

for i in range(n, n*11, n):
	inputSizes.append(i)
	randomArray = random.sample(range(2*i), i)
	randomArray.sort() #Best case for merge sort is if the input list/array is already sorted in ascending or descending order
	bestCaseArray = randomArray
	start = time()
	mergeSort(bestCaseArray)
	end = time()
	timeTaken = end - start
	mergeBestTimes.append(timeTaken)
	print(timeTaken, "seconds taken by Merge Sort in best case scenario for size ", i)
	

plt.figure(1)
plt.plot(inputSizes,mergeBestTimes, label = 'Best Case')
plt.legend(loc = 'upper center', shadow = True, fontsize = 'large')
plt.xlabel('Size')
plt.ylabel('Time in seconds')
plt.title('Merge Sort Time for Sorted Data')


n = 10000
i = 0
inputSizes = []
mergeSortTimes = []

for i in range(n, n*11, n):
	inputSizes.append(i)
	randomArray = random.sample(range(2*i), i)
	start = time()
	mergeSort(randomArray)
	end = time()
	timeTaken = end - start
	mergeSortTimes.append(timeTaken)
	print(timeTaken, "seconds taken by Merge Sort for size ", i)

plt.figure(2)
plt.plot(inputSizes,mergeSortTimes, label = 'Merge Sort')
plt.legend(loc = 'upper center', shadow = True, fontsize = 'large')
plt.xlabel('Size')
plt.ylabel('Time in seconds')
plt.title('Merge Sort Time for random data')
plt.show()