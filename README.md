COMP 314: Algorithms and Complexity

Lab work 2: Sorting

Tasks
1. Implement the following sorting algorithms:
(a) Insertion sort
(b) Merge sort
2. Write some test cases to test your program.
3. Generate some random inputs for your program and apply both insertion
sort and merge sort algorithms to sort the generated sequence of data.
Record the execution times of both algorithms for best and worst cases
on inputs of different size. Plot an input-size vs execution-time graph.
4. Explain your observations.
