import unittest
from insertionSort import insertion

class SortTestCase(unittest.TestCase):
	
# Test case for Insertion Sort
	def test_insertionSort(self):
		list = [1, 4, 3, 9, 7, 10, 19, 13, 12, 14]
		sortedList = [1, 3, 4, 7, 9, 10, 12, 13, 14, 19]
		self.assertEqual(insertion(list), sortedList)


if __name__ == '__main__':
	unittest.main()