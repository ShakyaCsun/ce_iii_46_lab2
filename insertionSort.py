def insertion(list):
	for j in range (1, len(list)):
		key = list[j]
		i=j-1
		while i>=0 and list[i] > key:
			list[i+1] = list[i]
			i = i - 1
		list[i+1] = key
	return list


if __name__ == '__main__':
	List = [5,4,3,2,1]
	print("Elements in unsorted list:", List)
	sortedList = insertion(List)
	print("Elements in sorted list:", sortedList)