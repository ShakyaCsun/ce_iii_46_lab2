import unittest
from mergeSort import merge, mergeSort

class SortTestCase(unittest.TestCase):
	
# Test case for Merge Sort
	def test_mergeSort(self):
		list = [1, 4, 3, 9, 7, 10, 19, 13, 12, 14]
		sortedList = [1, 3, 4, 7, 9, 10, 12, 13, 14, 19]
		self.assertEqual(mergeSort(list), sortedList)

# Test case for Merge function
	def test_merge(self):
		L = [2, 4, 6, 8]
		R = [1, 3, 5, 7]
		mergedList = [1, 2, 3, 4, 5, 6, 7, 8]
		self.assertEqual(merge(L, R), mergedList)

if __name__ == '__main__':
	unittest.main()