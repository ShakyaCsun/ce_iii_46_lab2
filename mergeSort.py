def merge(L, R):
	merged = []
	i = j = 0
	n1 = len(L)
	n2 = len(R)
	while (i < n1 and j < n2):
		if(L[i]<R[j]):
			merged.append(L[i])
			i += 1
		else:
			merged.append(R[j])
			j +=1
	while (i<n1):
		merged.append(L[i])
		i += 1
	while (j<n2):
		merged.append(R[j])
		j += 1
	return merged

def mergeSort(values):
	n = len(values)
	
	if n == 1:
		return values
	mid = n//2
	L = values[:mid]
	R = values[mid:]
	L = mergeSort(L)
	R = mergeSort(R)

	values = merge(L, R)
	return values

if __name__ == '__main__':
	List = [5,4,3,2,7]
	print("Elements in unsorted list:", List)
	sortedList = mergeSort(List)
	print("Elements in sorted list:", sortedList)
