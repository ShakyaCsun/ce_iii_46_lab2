from insertionSort import insertion
import random
from time import time
import matplotlib.pyplot as plt

n = 10000
i = 0
inputSizes = []
insertionBestTimes = []

for i in range(n, n*11, n):
	inputSizes.append(i)
	randomArray = random.sample(range(2*i), i)
	randomArray.sort() #Best case for insertion sort occurs when the list is already sorted in ascending order
	bestCaseArray = randomArray
	start = time()
	insertion(bestCaseArray)
	end = time()
	timeTaken = end - start
	insertionBestTimes.append(timeTaken)
	print(timeTaken, "seconds taken by Insertion Sort in best case scenario for size ", i)
	

plt.figure(1)
plt.plot(inputSizes,insertionBestTimes, label = 'Best Case')
plt.legend(loc = 'upper center', shadow = True, fontsize = 'large')
plt.xlabel('Size')
plt.ylabel('Time in seconds')
plt.title('Insertion Sort Time for sorted data')


n = 1000
i = 0
inputSizes = []
insertionWorstTimes = []

for i in range(n, n*11, n):
	inputSizes.append(i)
	randomArray = random.sample(range(2*i), i)
	randomArray.sort(reverse = True) #Worst case for insertion sort is if the input list is sorted in descending order
	worstCaseArray = randomArray
	start = time()
	insertion(worstCaseArray)
	end = time()
	timeTakenWorst = end - start
	insertionWorstTimes.append(timeTakenWorst)
	print(timeTakenWorst, "seconds taken by Insertion Sort in worst case scenario for size ", i)

plt.figure(2)
plt.plot(inputSizes,insertionWorstTimes, label = 'Worst Case')
plt.legend(loc = 'upper center', shadow = True, fontsize = 'large')
plt.xlabel('Size')
plt.ylabel('Time in seconds')
plt.title('Insertion Sort Time for reverse sorted data')
plt.show()